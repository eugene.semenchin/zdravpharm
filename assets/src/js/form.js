// form validation // 

const forms = document.querySelectorAll('form');

forms.forEach((form) => {
    const inputs = form.querySelectorAll('input');
    const button = form.querySelector('button');
    const validFormArr = [];
    
    inputs.forEach((input) => {
        if (input.hasAttribute("data-reg")) { // .jsValid
            input.setAttribute("is-valid", "false");
            validFormArr.push(input);
            input.addEventListener("input", inputHandler);
        }
    })
    
    button.addEventListener("click", () => {
        const inputsLength = validFormArr.length;
        const inputsOnlyValid = validFormArr.filter((input) => input.getAttribute('is-valid') === "true");
        
       
        if (inputsLength !== inputsOnlyValid.length) {
            // is not valid
            console.log('Форма не валидна');
            
        } else {
            // is valid
            console.log('Форма валидна');
            form.classList.add('form__send');
            let formData = new FormData(form);
            let xhr = new XMLHttpRequest();

            xhr.onreadystatechange = function () {
              if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                  console.log('Заявка отправлена!');
                }
              }
            }
        
            xhr.open('POST', '../plugins/mail.php', true);
            xhr.send(formData);
        
            formData.target.reset();
        }
    });

});

function inputHandler({ target }) {
    const inputValue = target.value;
    const inputReg = target.getAttribute("data-reg");
    const reg = new RegExp(inputReg);
    if (reg.test(inputValue)) {
        target.setAttribute("is-valid", "true");
        target.style.border = "2px solid #81bc00";
    } else {
        target.setAttribute("is-valid", "false");
        target.style.border = "2px solid #ff0000";
    }
}








