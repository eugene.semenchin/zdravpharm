
// swiper slider //

var swiper = new Swiper(".slider", {
    spaceBetween: 100,
    speed: 500,
    effect: 'fade',
    pagination: {
        el: ".swiper-pagination",
        dynamicBullets: true,
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
  });