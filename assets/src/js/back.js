
// back to top // 

const backToTop = document.querySelector('.back-to-top');
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 2500 || document.documentElement.scrollTop > 2500) {
        backToTop.classList.add('back-to-top--show');
    } else {
        backToTop.classList.remove('back-to-top--show');
    }
}

function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}