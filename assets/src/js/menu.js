
// burger menu //

const burgerButton = document.querySelector('.burger__mobile');
const burgerMenu = document.querySelector('.header__bottom');
const menuButton = document.querySelector('.close__menu');

const menuShow = function () {
    burgerMenu.classList.toggle('header__bottom--show');
}

const menuHide = function () {
    burgerMenu.classList.remove('header__bottom--show');
}

burgerButton.addEventListener('click', function () {
    menuShow();
})

menuButton.addEventListener('click', function () {
    menuHide();
})


