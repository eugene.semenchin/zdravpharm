
// global modals variables // 

const windowModals = document.querySelectorAll('.window__modal')
const closeModals = document.querySelectorAll('.close__modal')

const windowHide = function () {

    windowModals.forEach(function (window) {
        window.classList.remove('active');
    });

    overlay.classList.remove('overlay--active')
}

closeModals.forEach(function (close) {
    close.addEventListener("click", function () {
        windowHide();
    });
});
  

// survey - модальное окно с опросом // 

const surveyButton = document.querySelector('.calculation__button');
const surveyModal = document.querySelector('.survey__modal');

const surveyShow = function () {
    surveyModal.classList.toggle('active');
    overlay.classList.toggle('overlay--active')
}

surveyButton.addEventListener('click', function () {
    surveyShow();
})

// survey main settings // 

const SurveyJson = {
    "title": "Опрос для заказа",
    "description": "Пройдите опрос из 5 вопросов и получите расчет стоимости вашей партии, консультацию специалиста по производству",
    "logoPosition": "right",
    "focusOnFirstError": false,
    "completedHtml": "<h3>Спасибо, наш специалист свяжется с вами!</h3>",
    "loadingHtml": "<h3>Загрузка опроса</h3>",
    "pages": [
     {
      "name": "Страница 1",
      "elements": [
       {
        "type": "radiogroup",
        "name": "Направление",
        "title": "Какое направление планируете запустить в разработку?",
        "isRequired": true,
        "requiredErrorText": "Выберите один из предложенных вариантов",
        "choices": [
         "Витамины",
         "БАД для женщин",
         "БАД для мужчин",
         "Жиросжигатели",
         "БАД для похудения",
         "Витаминный комплекс",
         "Пищевая добавка"
        ],
        "showOtherItem": true,
        "otherText": "Напишите свой ответ",
        "otherErrorText": "Поле должно быть заполнено"
       }
      ],
      "questionTitleLocation": "hidden",
      "title": "Какое направление планируете запустить в разработку?",
      "description": "Выберите один их ответов или напишите свой"
     },
     {
      "name": "Страница 2",
      "elements": [
       {
        "type": "radiogroup",
        "name": "Форма",
        "title": "Какая форма выпуска будет у продукта?",
        "isRequired": true,
        "requiredErrorText": "Выберите один из предложенных вариантов",
        "choices": [
         "Таблетки",
         "Капсулы",
         "Блистеры",
         "Флаконы",
         "Пластиковые ампулы"
        ],
        "showOtherItem": true,
        "otherText": "Напишите свой ответ",
        "otherErrorText": "Поле должно быть заполнено"
       }
      ],
      "questionTitleLocation": "hidden",
      "title": "Какая форма выпуска будет у продукта?",
      "description": "Выберите один их ответов или напишите свой"
     },
     {
      "name": "Страница 3",
      "elements": [
       {
        "type": "radiogroup",
        "name": "Основа",
        "title": "На какой основе будем работать?",
        "isRequired": true,
        "requiredErrorText": "Выберите один из предложенных вариантов",
        "choices": [
         "Товар под ключ",
         "Фасовка на давальческой основе",
         "Затрудняюсь ответить, нужна консультация специалиста"
        ],
        "showOtherItem": true,
        "otherText": "Напишите свой ответ",
        "otherErrorText": "Поле должно быть заполнено"
       }
      ],
      "questionTitleLocation": "hidden",
      "title": "На какой основе будем работать?",
      "description": "Выберите один их ответов или напишите свой"
     },
     {
      "name": "Страница 4",
      "elements": [
       {
        "type": "text",
        "name": "Количество",
        "title": "от 1000 до 20000",
        "startWithNewLine": false,
        "titleLocation": "top",
        "hideNumber": true,
        "clearIfInvisible": "onComplete",
        "isRequired": true,
        "requiredErrorText": "Укажите нужное количество товаров",
        "inputType": "number",
        "autocomplete": "cc-number",
        "min": 1000,
        "max": 20000,
        "minErrorText": "Минимальный заказ от {0}",
        "maxErrorText": "Максимальный заказ от {0}",
        "step": 1
       }
      ],
      "questionTitleLocation": "hidden",
      "title": "Сколько товаров вы хотите заказать?",
      "description": "Укажите количество товара"
     },
     {
      "name": "Страница 5",
      "elements": [
       {
        "type": "radiogroup",
        "name": "Регистрация",
        "title": "Нужна будет помощь с регистрацией продукта?",
        "isRequired": true,
        "requiredErrorText": "Выберите один из предложенных вариантов",
        "choices": [
         "Регистрация не нужна",
         "Да, нужна помощь в регистрации"
        ],
        "showOtherItem": true,
        "otherText": "Напишите свой ответ",
        "otherErrorText": "Поле должно быть заполнено"
       }
      ],
      "questionTitleLocation": "hidden",
      "title": "Нужна будет помощь с регистрацией продукта?",
      "description": "Выберите один их ответов или напишите свой"
     },
     {
      "name": "Страница 6",
      "elements": [
       {
        "type": "text",
        "name": "Имя",
        "title": "Имя",
        "hideNumber": true,
        "isRequired": true,
        "requiredErrorText": "Введите ваше реальное имя",
        "autocomplete": "username",
        "placeholder": "Введите ваше имя"
       },
       {
        "type": "text",
        "name": "Почта",
        "title": "Почта",
        "hideNumber": true,
        "isRequired": true,
        "requiredErrorText": "Введите настоящий адрес почты",
        "inputType": "email",
        "autocomplete": "email",
        "placeholder": "Введите вашу почту"
       },
       {
        "type": "text",
        "name": "Телефон",
        "title": "Телефон",
        "hideNumber": true,
        "isRequired": true,
        "requiredErrorText": "Введите верный номер телефона",
        "inputType": "tel",
        "autocomplete": "tel",
        "placeholder": "Введите ваш номер телефона"
       }
      ],
      "title": "Получить расчет",
      "description": "Заполните форму, чтобы менеджер смог связаться с вами"
     }
    ],
    "showPageNumbers": true,
    "checkErrorsMode": "onValueChanging",
    "pagePrevText": "Назад",
    "pageNextText": "Далее",
    "completeText": "Отправить",
    "widthMode": "responsive"
}

const survey = new SurveyKnockout.SurveyModel(SurveyJson);
survey.onComplete.add((sender, options) => {
    console.log(sender.data);

    let formData = new FormData();

    for(let key in sender.data) { 
        let value = sender.data[key]; 
        formData.append(key, value);
    }

    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
        if (xhr.status === 200) {
            console.log('Заявка отправлена!');
        }
        }
    }

    xhr.open('POST', '../plugins/mail.php', true);
    xhr.send(formData);
});

ko.applyBindings({
    model: survey
}, document.getElementById("surveyElement"));

// consultation - модальное окно консультация - форма // 

const consultationButton = document.querySelector('.consultation__button');
const consultationModal = document.querySelector('.consultation__modal');

const consultationShow = function () {
    consultationModal.classList.toggle('active');
    overlay.classList.toggle('overlay--active');
}

consultationButton.addEventListener('click', function () {
    consultationShow();
});


// contract - модальное окно договор - форма //

const contractButton = document.querySelector('.contract__button');
const contractModal = document.querySelector('.contract__modal');

const contractShow = function () {
    contractModal.classList.toggle('active');
    overlay.classList.toggle('overlay--active');
}

contractButton.addEventListener('click', function () {
    contractShow();
});